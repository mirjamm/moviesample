﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models {
	public class Movie {
		public int Id { get; set; }	
		public int Year { get; set; }
		public string Description { get; set; }
		public int Rating { get; set; }
		public int CategoryId { get; set; }
		public string Title { get; set; }	
		public Category Category { get; set; }
	}
}
