﻿using Data.Models;
using Data.Repositories.Interfaces;
using Data.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Services {
	public class CategoryService: ICategoryService {
		private readonly ICategoryRepository _categoryRepository;

		public CategoryService(ICategoryRepository categoryRepository) {
			_categoryRepository = categoryRepository;
		}
		public IEnumerable<Category> GetAll() {
			return _categoryRepository.GetAll();
		}
	}
}
