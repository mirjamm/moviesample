﻿using Data.Models;
using Data.Repositories.Interfaces;
using Data.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Services {
	public class MovieService: IMovieService {
		private readonly IMovieRepository _movieRepository;
		private readonly ICategoryRepository _categoryRepository;

		public MovieService(IMovieRepository movieRepository, ICategoryRepository categoryRepository) {
			_movieRepository = movieRepository;
			_categoryRepository = categoryRepository;
		}

		public IEnumerable<Movie> GetAll() {
			var movies = _movieRepository.GetAll();
			foreach (var movie in movies) {
				movie.Category = _categoryRepository.GetById(movie.CategoryId) ?? new Category();
			}
			return movies;
		}

		public Movie GetById(int id) {
			var movie = _movieRepository.GetById(id);
			movie.Category = _categoryRepository.GetById(movie.CategoryId) ?? new Category();
			return movie;
		}
	}
}
