﻿using Data.Models;
using Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories {
	public class MovieRepository: IMovieRepository {
		public IEnumerable<Movie> GetAll() {
			List<Movie> movies = new List<Movie>() {
			new Movie(){
				Id=1,
				Title="Test1",
				Description="Test 1",
				Rating=1,
				Year=1991,
				CategoryId=1
			},
			new Movie(){
				Id=1,
				Title="Test2",
				Description="Test 2",
				Rating=2,
				Year=2002,
				CategoryId=1
			},
			new Movie(){
				Id=1,
				Title="Test3",
				Description="Test 3",
				Rating=3,
				Year=2013,
				CategoryId=2
			},
			new Movie(){
				Id=1,
				Title="Test4",
				Description="Test 4",
				Rating=4,
				Year=1994,
				CategoryId=4
			}
		};
			return movies;
		}

		public Movie GetById(int id) {
			return GetAll().FirstOrDefault(m => m.Id == id) ?? new Movie();
		}
	}
}
