﻿using Data.Models;
using Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories {
	public class CategoryRepository: ICategoryRepository {
		public IEnumerable<Category> GetAll() {
			List<Category> categories = new List<Category>() {
			new Category(){
				Id=1,
				Name="Action"
			} ,
			new Category(){
				Id=1,
				Name="Test 2"
			}
			};
			return categories;
		}

		public Category GetById(int id) {
			return GetAll().FirstOrDefault(m => m.Id == id) ?? new Category();
		}
	}
}
