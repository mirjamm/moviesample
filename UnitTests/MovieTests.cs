using Data.Models;
using Data.Repositories.Interfaces;
using Data.Services;
using Data.Services.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace UnitTests {
	[TestClass]
	public class MovieTests {

		List<Movie> dummyMovies = new List<Movie>{
			new Movie(){
				Id=1,
				Title="Test1",
				Description="Test 1",
				Rating=1,
				Year=1991,
				CategoryId=1
			},
			new Movie(){
				Id=1,
				Title="Test2",
				Description="Test 2",
				Rating=2,
				Year=2002,
				CategoryId=1
			},
			new Movie(){
				Id=1,
				Title="Test3",
				Description="Test 3",
				Rating=3,
				Year=2013,
				CategoryId=2
			},
			new Movie(){
				Id=1,
				Title="Test4",
				Description="Test 4",
				Rating=4,
				Year=1994,
				CategoryId=4
			}
			};
		private Mock<IMovieRepository> _movieRepository = new Mock<IMovieRepository>();
		private Mock<ICategoryRepository> _categoryRepository = new Mock<ICategoryRepository>();
		private MovieService _movieService;

		public MovieTests() {
			_movieService = new MovieService(_movieRepository.Object, _categoryRepository.Object);
		}

		[TestMethod]
		public void GetAll_LenghtIs4() {
			int expectedCount = 4;

			_movieRepository.Setup(m => m.GetAll()).Returns(dummyMovies);

			var result = _movieService.GetAll().ToList();

			Assert.AreEqual(result.Count, expectedCount);
		}

		[TestMethod]
		public void GetById_TitleIsTest1() {
			string expected = "Test1";

			_movieRepository.Setup(m => m.GetById(1)).Returns(dummyMovies[0]);

			var result = _movieService.GetById(1).Title;

			Assert.AreEqual(result, expected);
		}

		[TestMethod]
		public void GetById_TitleIsNotTest2() {
			string expected = "Test2";

			_movieRepository.Setup(m => m.GetById(1)).Returns(dummyMovies[0]);

			var result = _movieService.GetById(1).Title;

			Assert.AreNotEqual(result, expected);
		}
	}
}