This project needs IDEs to run. Visual Studio for backend and Visual Studio Code for frontend.

Cloning repository
git clone https://gitlab.com/mirjamm/moviesample.git

Open moviesample> MovieProject.sln in Visual Studio and run it.

Open moviesample> MovieProject> ClientApp in VS Code.
run npm install to install modules.
run npm start to start the application frontend.
Application will start on https://localhost:44412/