
export interface Movie {
    id: number;
    year: number;
    description: string;
    rating: number;
    title: string;
    categoryName: string;
}

export interface Category {
    id: number;
    name: string;
}