import { ThrowStmt } from '@angular/compiler';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { IComboSelectionChangingEventArgs, IgxDropDownBaseDirective, IgxGridComponent, IgxInputDirective, ISelectionEventArgs } from 'igniteui-angular';
import { Subscription } from 'rxjs';
import { CategoryService } from 'src/app/services/category/category.service';
import { MovieService } from 'src/app/services/movie/movie.service';
import { Category, Movie } from 'src/app/shared/model';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css']
})
export class MovieComponent implements OnInit, OnDestroy {
  @ViewChild('grid1', { read: IgxGridComponent })
  public grid1: IgxGridComponent | undefined;
  @ViewChild('movieInput', { read: IgxInputDirective })
  public movieInput: IgxInputDirective | undefined;


  public movies: Movie[] = [];
  public moviesData: Movie[] = [];
  public filteredMovies: Movie[] = [];
  public movieSearch: string | undefined;
  public selectedCategories: string[] = [];
  public categoriesData: Category[] = [];
  private subscriptions: Subscription[] = [];

  constructor(private movieService: MovieService, private categoryService: CategoryService) {
    this.getMovies();
    this.getCategories();
  }


  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe);
  }

  ngOnInit(): void {

  }

  public getMovies() {
    this.subscriptions.push(this.movieService.getMovies().subscribe(result => {
      this.movies = result;
      this.moviesData = result;
    })
    );
  }

  public getCategories() {
    this.subscriptions.push(this.categoryService.getCategories().subscribe(result => {
      this.categoriesData = result;
    })
    );
  }

  public changeMovie(event: any) {
    if (event.title !== undefined) {
      if (this.movieInput !== undefined) {
        this.movieInput.nativeElement.value = event.title;
        this.movieSearch = event.title;
        this.filterGridData();
      }
    } else {
      this.getFilteredMovies(event);
    }
  }

  private getFilteredMovies(title: string) {
    if (this.movies !== undefined) {
      this.filteredMovies = this.movies.filter((item) => item.title.toString().toLowerCase().includes(title.toString().toLowerCase()));
      this.movieSearch = title;
      this.filterGridData();
    }
  }

  public movieDropdownSelectionHandler(eventArgs: ISelectionEventArgs) {
    if (this.movieInput !== undefined) {
      this.movieInput.nativeElement.value = eventArgs.newSelection.value.title;

    }
    eventArgs.cancel = true;
  }

  private filterGridData() {
    if (this.selectedCategories.length > 0) {
      this.filterGridDataByCategory(this.selectedCategories);
    } else {
      this.moviesData = this.movies;
    }
    if (this.movieSearch !== undefined && this.movieSearch !== '') {
      this.moviesData = this.moviesData.filter((item) => item.title.toLowerCase().includes(this.movieSearch!.toLowerCase()));
    }
  }

  public handleCategorySelection(event: IComboSelectionChangingEventArgs) {
    let selection = this.selectedCategories;
    for (const item of event.added) {
      selection.push(item);
    }
    for (const item of event.removed) {
      let index = this.selectedCategories.indexOf(item);
      if (index > -1) {
        selection.splice(index, 1);
      }
    }
    this.selectedCategories = selection;
    this.filterGridData();
  }

  private filterGridDataByCategory(categories: string[]) {
    let data: Movie[] = [];
    categories.forEach(c => {
      this.movies.forEach(element => {
        if (element.categoryName?.toLowerCase() == c.toLowerCase()) {
          data.push(element);
        }
      })
    });
    this.moviesData = data;
  }
}
