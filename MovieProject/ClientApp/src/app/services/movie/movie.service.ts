import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Movie } from 'src/app/shared/model';

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) { }

  getMovies(): Observable<Movie[]> {
    return this.http.get<Movie[]>(this.baseUrl + 'api/movie')
  }
}
