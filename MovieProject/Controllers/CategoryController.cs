﻿using Data.Models;
using Data.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MovieProject.Controllers {
	[ApiController]
	[Route("api/[controller]")]
	public class CategoryController: Controller {
		private readonly ICategoryService _categoryService;

		public CategoryController(ICategoryService categoryService) {
			_categoryService = categoryService;
		}

		[HttpGet]
		public List<Category> GetCategories() {
			return _categoryService.GetAll().ToList();
		}
	}
}
