﻿using Data.Models;
using Data.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MovieProject.Models;

namespace MovieProject.Controllers {
	[ApiController]
	[Route("api/[controller]")]
	public class MovieController: Controller {
		private readonly IMovieService _movieService;

		public MovieController(IMovieService movieService) {
			_movieService = movieService;
		}

		[HttpGet]
		public List<MovieViewModel> GetMovies() {
			return _movieService.GetAll().Select(m => new MovieViewModel {
				Id = m.Id,
				Description = m.Description,
				CategoryName = m.Category.Name,
				Rating = m.Rating,
				Title = m.Title,
				Year = m.Year
			}).ToList();
		}

		[HttpGet("{id}")]
		public MovieViewModel GetMovie(int id) {
			var movie = _movieService.GetById(id);
			return new MovieViewModel {
				Id = movie.Id,
				Description = movie.Description,
				CategoryName = movie.Category.Name,
				Rating = movie.Rating,
				Title = movie.Title,
				Year = movie.Year
			};
		}
	}
}
