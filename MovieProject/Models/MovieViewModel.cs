﻿namespace MovieProject.Models {
	public class MovieViewModel {
		public int Id { get; set; }
		public int Year { get; set; }
		public string Description { get; set; }
		public int Rating { get; set; }
		public string Title { get; set; }
		public string CategoryName { get; set; }
	}
}
